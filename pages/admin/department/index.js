import React, { useEffect, useRef, useState } from "react";
import {
  Box,
  Button,
  Center,
  Container,
  createStandaloneToast,
  Flex, FormControl, FormErrorMessage, FormLabel,
  Input,
  InputGroup,
  InputRightElement,
  Modal,
  ModalBody,
  ModalCloseButton,
  ModalContent, ModalFooter,
  ModalHeader,
  ModalOverlay,
  Spacer,
  Table,
  Tbody,
  Td,
  Text, Textarea,
  Th,
  Thead,
  Tr,
  useDisclosure,
  Wrap,
  WrapItem
} from "@chakra-ui/react";
import { ViewIcon, ViewOffIcon } from "@chakra-ui/icons";
import { useFormik } from "formik";
import adminApi from "../../../services/adminApi";
import { getMessage, isErrorResponse } from "../../../utils/typeof";
import ModalComponent from "../../../components/ModalComponent";

const Department = (props) => {
  const toast = createStandaloneToast();

  const [listDepartment, setListDepartment] = useState([]);
  const [listUser, setListUser] = useState([]);
  const [idSelected, setIdSelected] = useState(null);
  const [openUpdateModal, setOpenUpdateModal] = useState(false);
  const [openCreateModal, setOpenCreateModal] = useState(false);
  const [initValueForm, setInitValueForm] = useState({});

  const getListDepartment = () => {
    adminApi.getListDepartment()
      .then((res) => {
        if (isErrorResponse(res)) {
          console.log(getMessage(res));
        } else {
          setListDepartment(res.data);
        }
      })
  }

  const getListUser = () => {
    adminApi.getListUser()
      .then((res) => {
        if (isErrorResponse(res)) {
          console.log(getMessage(res));
        } else {
          setListUser(res.data);
        }
      })
  }

  useEffect(() => {
    getListDepartment();
    getListUser();
  }, []);

  const validate = (value) => {
    const errors = {};
    if (!value.name) {
      errors.name = "Tên phòng ban không được bỏ trống";
    }

    if (!value.manager) {
      errors.manager = "Mời chọn trưởng phòng";
    }
    if (!value.mission) {
      errors.mission = "Sứ mệnh của phòng ban không được bỏ trống";
    }

    if (!value.location) {
      errors.location = "Vị trí của phòng ban không được bỏ trống"
    }

    return errors;
  };

  const removeDepartment = (params) => {
    const departmentId = params.id;

    adminApi.removeDepartment(departmentId)
      .then((res) => {
        if (isErrorResponse(res)) {
          getListDepartment();
          toast({
            title: "Đã xong",
            render: "top",
            description: "Xóa phòng ban thành công",
            status: "success",
            duration: 3000,
            isClosable: true,
          })
        }
      })
      .catch((err) => {
        console.log(err);
      })

  };


  const updateDepartment = (department) => {
    setIdSelected(department._id);
    setInitValueForm({
      name: department.name,
      manager: department.user?._id,
      mission: department.mission,
      location: department.location,
    });
    setOpenUpdateModal(true);
  };

  const createDepartment = () => {
    setInitValueForm({
      name: '',
      manager: '',
      mission: '',
      location: '',
    })
    setOpenCreateModal(true);
  };

  const submitUpdateDepartment = (department) => {
    const param = {department_id: idSelected, ...department}
    adminApi.updateDepartment(param)
      .then((res) => {
        const list = [...listDepartment];
        list.push(res.data);
        toast({
          title: "Đã xong",
          render: "top",
          description: "Đã cập nhập phòng ban",
          status: "success",
          duration: 3000,
          isClosable: true,
        })
        setListDepartment(list);
      })
      .catch((err) => {
        console.log(err);
      })
  };

  const submitCreateDepartment = (department) => {
    adminApi.createDepartment(department)
      .then((res) => {
        if (res.data.code !== 201) {
          getListDepartment();
          toast({
            title: "Đã xong",
            render: "top",
            description: "Đã tạo mới phòng ban",
            status: "success",
            duration: 3000,
            isClosable: true,
          })
        }
      })
      .catch((err) => {
        console.log(getMessage(err))
      })
  };

  return (
    <Container
      w={'container.xl'}
      maxWidth={'100%'}
      h={'100vh'}
      pt={'100px'}
    >
      <Container
        w={'100%'}
        maxWidth={'100%'}
        position={'sticky'}
        top={'60px'}
        bg={'gray.100'}
      >
        <Flex
          direction={'row'}
          w={'95%'}
          maxWidth={'100%'}
          justify={'space-between'}
        >
          <Box>
            <Text
              fontSize={'5xl'}
              fontWeight={'bold'}
              p={2}
            >
              Danh sách phòng ban
            </Text>
          </Box>
          <Spacer/>
          <Center>
            <Button
              colorScheme={'twitter'}
              onClick={() => createDepartment()}
              mr={4}
            >
              Thêm phòng ban
            </Button>
          </Center>
        </Flex>
      </Container>
      <Container
        w={'100%'}
        maxWidth={'95%'}
        mt={3}
      >
        <Table
          size={'md'}
          colorScheme={'gray'}
        >
          <Thead>
            <Tr>
              <Th>STT</Th>
              <Th>Tên phòng ban</Th>
              <Th>Trưởng phòng</Th>
              <Th>Sứ mệnh</Th>
              <Th>Vị trí</Th>
              <Th></Th>
              <Th></Th>
            </Tr>
          </Thead>
          <Tbody>
            {
              listDepartment && listDepartment.map((department, index) => (
                <Tr
                  key={`department-${index}`}
                  _hover={{
                    background: 'gray.100'
                  }}
                >
                  <Td>{index + 1}</Td>
                  <Td><Text cursor={'pointer'}>{department.name}</Text></Td>
                  <Td><Text cursor={'pointer'}>{department.user?.full_name}</Text></Td>
                  <Td>{department.mission}</Td>
                  <Td>{department.location}</Td>
                  <Td
                    cursor={'pointer'}
                    color={'green'}
                    _hover={{
                      textDecoration: "underline"
                    }}
                    onClick={() => updateDepartment(department)}
                  >Sửa</Td>
                  <Td
                    cursor={'pointer'}
                    color={'red'}
                    _hover={{
                      textDecoration: "underline"
                    }}
                    onClick={() => removeDepartment({id: department._id})}
                  >
                    Xóa
                  </Td>
                </Tr>
              ))
            }
          </Tbody>
        </Table>
      </Container>
      <ModalComponent
        title={'Cập nhập phòng ban'}
        titleSubmit={'Cập nhập'}
        openForm={openUpdateModal}
        initValue={initValueForm}
        closeForm={() => setOpenUpdateModal(false)}
        validateValue={validate}
        fields={[
          {key: 'name', title: 'Tên phòng ban:'},
          {
            key: 'manager',
            display: 'full_name',
            title: 'Người quản lý:',
            placeholder: 'Chọn trưởng phòng',
            type: 'selector',
            data: listUser
          },
          {key: 'mission', title: 'Sứ mệnh phòng ban:'},
          {key: 'location', title: 'Vị trí phòng ban:'},
        ]}
        onSubmit={submitUpdateDepartment}
      />

      <ModalComponent
        title={'Thêm phòng ban mới'}
        titleSubmit={'Tạo phòng ban'}
        openForm={openCreateModal}
        initValue={initValueForm}
        closeForm={() => setOpenCreateModal(false)}
        validateValue={validate}
        fields={[
          {key: 'name', title: 'Tên phòng ban:'},
          {
            key: 'manager',
            display: 'full_name',
            title: 'Người quản lý:',
            placeholder: 'Chọn trưởng phòng',
            type: 'selector',
            data: listUser
          },
          {key: 'mission', title: 'Sứ mệnh phòng ban:'},
          {key: 'location', title: 'Vị trí phòng ban:'},
        ]}
        onSubmit={submitCreateDepartment}
      />
    </Container>
  )
};


export default Department;