import React, { useEffect, useState } from 'react';
import { Box, Button, Center, Container, Flex, Input, Text, useEditable, useToast } from "@chakra-ui/react";
import { useDispatch, useSelector } from "react-redux";
import userApi from "../../services/userApi";
import { getMessage, isError, isErrorResponse } from "../../utils/typeof";
import { setInfo } from "../../redux/actions/user";
import { getValueFromDate } from "../../utils/formatTime";

const UserProfile = (props) => {

  const user = useSelector(state => state.user.infoUser || null);

  const toast = useToast();
  const dispatch = useDispatch();

  const [isSubmit, setIsSubmit] = useState(false);
  const [address, setAddress] = useState(user?.address);
  const [dateOfBirth, setDateOfBirth] = useState(user?.date_of_birth);
  const [numberPhone, setNumberPhone] = useState(user?.number_phone);

  useEffect(() => {
    if (address !== user?.address || dateOfBirth !== user?.date_of_birth || numberPhone !== user?.number_phone) {
      setIsSubmit(true);
    } else {
      setIsSubmit(false)
    }
  }, [address, dateOfBirth, numberPhone])
  const handleUpdate = () => {
    const newUser = {
      ...user,
      number_phone: numberPhone,
      address: address,
      date_of_birth: dateOfBirth
    }

    userApi.updateProfile(newUser)
      .then((res) => {
        if (isErrorResponse(res) && isError(res)) {
          console.log(getMessage(res));
          toast({
            title: "Lỗi",
            description: "Cập nhập thông tin thất bại.",
            status: "error",
            duration: 3000,
            isClosable: true,
          })
        } else {
          dispatch(setInfo(res.data))
          toast({
            title: "Đã lưu",
            render: "top",
            description: "Cập nhập thông tin thành công.",
            status: "success",
            duration: 3000,
            isClosable: true,
          })
        }
      })
      .catch((err) => {
        console.log(getMessage(err));
      })
  }
  return (
    <Container
      w={'container.xl'}
      maxWidth={'100%'}
      h={'100vh'}
      pt={'50px'}
    >
      <Box>
        <Text fontSize={'5xl'} fontWeight={'bold'} p={'3rem'}>Hồ sơ người dùng</Text>
      </Box>

      <Container
        w={'100%'}
        maxWidth={'90%'}
      >

        <Flex w={'100%'} py={'1rem'} direction={'row'}>
          <Text
            w={'10rem'}
            fontSize={'lg'}
            fontWeight={'bold'}
          >
            Họ và tên:
          </Text>
          <Text maxWidth={'50rem'}>{user.full_name}</Text>
        </Flex>

        <Flex w={'100%'} py={'1rem'} direction={'row'}>
          <Text
            w={'10rem'}
            fontSize={'lg'}
            fontWeight={'bold'}
            m={'auto 0'}
          >
            Email:
          </Text>
          <Text maxWidth={'50rem'}>{user.email}</Text>
        </Flex>

        <Flex w={'100%'} py={'1rem'} direction={'row'}>
          <Text
            w={'10rem'}
            fontSize={'lg'}
            fontWeight={'bold'}
            m={'auto 0'}
          >
            Địa chỉ hiện tại:
          </Text>

          <Input
            w={'100%'}
            placeholder={'Địa chỉ tạm trú'}
            onChange={(e) => setAddress(e.target.value)}
            size={'md'}
            value={address}/>
        </Flex>

        <Flex w={'100%'} py={'1rem'} direction={'row'}>
          <Text w={'10rem'} fontSize={'lg'} fontWeight={'bold'} m={'auto 0'}>Ngày sinh: </Text>

          <Input
            w={'100%'}
            type={'date'}
            onChange={(e) => setDateOfBirth(e.target.value)}
            size={'md'}
            value={getValueFromDate(dateOfBirth)}/>
        </Flex>

        <Flex w={'100%'} py={'1rem'} direction={'row'}>
          <Text w={'10rem'} fontSize={'lg'} fontWeight={'bold'} m={'auto 0'}>Số điện thoại: </Text>

          <Input
            w={'100%'}
            onChange={(e) => setNumberPhone(e.target.value)}
            size={'md'}
            value={numberPhone}/>
        </Flex>

        <Center w={'100%'}>
          <Button
            colorScheme={'blue'}
            textAlign={'center'}
            h="1.7rem"
            mt={'3rem'}
            p={'1.5rem'}
            type={'submit'}
            isDisabled={isSubmit}
            onClick={handleUpdate}
          >
            Cập nhập
          </Button>
        </Center>
      </Container>
    </Container>
  );
};

export default UserProfile;