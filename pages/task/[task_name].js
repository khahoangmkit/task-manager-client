import React, { useEffect, useMemo, useState } from 'react';
import { Box, Button, Center, Container, Flex, Input, Text, Textarea, Wrap, WrapItem } from "@chakra-ui/react";
import { useToast } from "@chakra-ui/react";
import { useRouter } from "next/router";
import { getParamByRouter } from "../../utils/history";
import userApi from "../../services/userApi";
import { getMessage, isError, isErrorResponse } from "../../utils/typeof";

const TaskDetail = (props) => {

  const router = useRouter();
  const toast = useToast();

  const [task, setTask] = useState({});
  const [done, setDone] = useState(task?.done);
  const [isSubmit, setIsSubmit] = useState(false);
  const taskName = useMemo(() => getParamByRouter(router, 'task_name'), [router]);


  useEffect(() => {
    if (taskName) {
      userApi.getTaskByName(taskName).then((res) => {
        if (res.code !== 200) {
          console.log(res.message);
        } else {
          setTask(res.data);
          setDone(res.data.done);
        }
      })
    }
  }, [taskName]);

  useEffect(() => {
    setIsSubmit(task.done === done);
  }, [done])

  const updateTask = () => {
    userApi.updateTask({
      _id: task._id,
      done: done
    }).then((res) => {
      if (isErrorResponse(res) && isError(res) ) {
        console.log(getMessage(res));
        toast({
          title: "Lỗi",
          description: "Cập nhập tiến độ thất bại.",
          status: "error",
          duration: 3000,
          isClosable: true,
        })
      } else {
        setTask(res.data)
        toast({
          title: "Đã lưu",
          render: "top",
          description: "Cập nhập tiến độ thành công.",
          status: "success",
          duration: 3000,
          isClosable: true,
        })
      }
    })
  };

  const handleInputDone = (e) => {
    const done = e.target.value;
    if (done > task?.goal) {
      return;
    } else {
      setDone(done);
    }
  }
  return (
    <Container
      w={'container.xl'}
      maxWidth={'100%'}
      h={'100vh'}
      pt={'50px'}
    >
      <Box>
        <Text fontSize={'5xl'} fontWeight={'bold'} p={'3rem'}>Chi tiết công việc</Text>
      </Box>

      <Container
        w={'100%'}
        maxWidth={'90%'}
      >

        <Flex w={'100%'} py={'1rem'} direction={'row'}>
          <Text
            w={'10rem'}
            fontSize={'md'}
            fontWeight={'bold'}
            m={'auto 0'}
          >
            Tên công việc:
          </Text>
          <Text maxWidth={'50rem'}>{task.name}</Text>
        </Flex>

        <Flex w={'100%'} py={'1rem'} direction={'row'}>
          <Text
            w={'10rem'}
            fontSize={'md'}
            fontWeight={'bold'}
          >
            Mô tả:
          </Text>
          <Text maxWidth={'50rem'}>{task.description}</Text>
        </Flex>


        <Flex w={'100%'} py={'1rem'} direction={'row'}>
          <Text w={'10rem'} fontSize={'md'} fontWeight={'bold'} m={'auto 0'}>Đã hoàn thành: </Text>
          <Flex direction={'row'}>
            <Input
              w={'5rem'}
              onChange={handleInputDone}
              size={'md'}
              textAlign={'center'}
              value={done}/>
            <Text m={'auto 0'} pl={'.5rem'}> / {task.goal}</Text>
          </Flex>
        </Flex>

        <Flex w={'100%'} py={'1rem'} direction={'row'}>
          <Text
            w={'10rem'}
            fontSize={'md'}
            fontWeight={'bold'}
          >
            Hạn chót:
          </Text>
          <Text maxWidth={'50rem'}>19/07/2021</Text>
        </Flex>

        <Flex w={'100%'} py={'1rem'} direction={'row'}>
          <Text w={'10rem'} fontSize={'md'} fontWeight={'bold'} m={'auto 0'}>Ghi chú: </Text>
          <Flex direction={'row'}>
            <Textarea
              w={'50rem'}
              maxWidth={'50rem'}
              onChange={handleInputDone}
              size={'md'}
              value={''}/>
          </Flex>
        </Flex>

        <Center w={'100%'}>
          <Button
            colorScheme={'blue'}
            textAlign={'center'}
            h="1.7rem"
            mt={'3rem'}
            p={'1.5rem'}
            type={'submit'}
            isDisabled={isSubmit}
            onClick={updateTask}
          >
            Cập nhập
          </Button>
        </Center>
      </Container>
    </Container>
  )
};

export default TaskDetail;