import React, { useEffect, useState } from 'react';
import { Box, Container, Flex, Grid, Text } from "@chakra-ui/react";
import { useRouter } from "next/router";
import { useSelector } from "react-redux";

const SideBar = (props) => {

  const router = useRouter();
  const closeSideBar = props.close;

  const role = useSelector(state => state.user.role || null);


  const listMenu = role === 'admin' ? [
    {
      name: 'Quản lý người dùng',
      handle: '/admin/user'
    },
    {
      name: 'Quản lý phòng ban',
      handle: '/admin/department'
    },
    {
      name: 'Quản lý quyền',
      handle: '/admin/role'
    }
  ] : [
    {
      name: 'Cập nhập hồ sơ',
      handle: '/user'
    },
    {
      name: 'Đổi mật khẩu',
      handle: '/user/password'
    },
    {
      name: 'Đăng xuất',
      handle: '/user/logout'
    },
  ];

  return (
    <Grid
      w={'100%'}
      h={'100vh'}
      templateColumns={'300px auto'}
      position={'fixed'}
      top={0}
      left={0}
      zIndex={1000}
    >
      <Box bg={'#fff'}>
        {
          listMenu.map((item,index) =>
            <Text
              key={`item-${index}`}
              _hover={{
                background: "gray.100"
              }}
              cursor={'pointer'}
              p={'20px'}
              borderBottom={'2px solid #dfe6e9'}
              onClick={() => {
                router.push(item.handle, undefined, {shallow: true})
                closeSideBar()
              }
              }
            >
              {item.name}
            </Text>
          )
        }
      </Box>
      <Box bg={'#000'} opacity={0.1} onClick={() => {
        closeSideBar()
      }}/>

    </Grid>
  );
}

export default SideBar;
