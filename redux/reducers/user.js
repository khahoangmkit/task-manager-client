import { SET_ROLE, SET_TOKEN, SET_USER } from '../types/user';

const initialState = {
  infoUser: {},
  token: '',
  logged: false,
  role: ''
}

export default (state = initialState, action) => {
  switch (action.type) {
    case SET_USER:
      if (action.payload) {
        const infoUser = action.payload;

        const user = infoUser;

        return {
          ...state,
          infoUser: user,
          logged: true
        }
      }
      return state;

    case SET_TOKEN:
      if (action.payload) {
        const token = action.payload;

        return {
          ...state,
          token: token
        }
      }
      return state;

    case SET_ROLE:
      if(action.payload) {
        const role = action.payload;

        return {
          ...state,
          role: role
        }
      }
      return state;

    default:
      return state;
  }
}
