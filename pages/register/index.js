import { makeStyles } from "@material-ui/core/styles";
import React, { useState } from "react";
import { useRouter } from "next/router";
import userApi from "../../services/userApi";
import cookie from "cookie-cutter";
import { Avatar, Button, Container, CssBaseline, FormHelperText, Typography } from "@material-ui/core";
import TextField from "@material-ui/core/TextField";

const useStyles = makeStyles((theme) => ({
  paper: {
    marginTop: theme.spacing(8),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: '100%',
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
  textField: {
    marginLeft: theme.spacing(1),
    marginRight: theme.spacing(1),
    width: 200,
  }
}));

const Register = (props) => {

  const [fullName, setFullName] = useState('');
  const [userName, setUserName] = useState('');
  const [password, setPassword] = useState('');
  const [rePassword, setRePassword] = useState('');
  const [phone, setPhone] = useState('');
  const [email, setEmail] = useState('');
  const [birthday, setBirthday] = useState('');

  const router = useRouter();

  const signUp = (e) => {
    e.preventDefault();
    userApi.signUp({
      user_name: userName,
      password,
      full_name: fullName,
      date_of_birth: birthday,
      number_phone: phone,
      email
    }).then((res) => {
        if (res.data.code !== 201) {
          alert(res.data.message);
        } else {
          userApi.login({
            user_name: userName,
            password
          }).then((res) => {
            if (res.data.code !== 200) {
              alert(res.data.message);
            } else {
              const token = res.data.token;
              cookie.set('token', token, {expires: 60 * 3600});
              router.push('/');
            }
          })
        }
      })
      .catch((err) => {
        console.log(err.message)
      })
  };

  const emailValidate = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  const phoneNumberValidate = /[0]\d{9}$/g;

  const classes = useStyles();
  return (
    <Container component="main" maxWidth="xs">
      <CssBaseline/>
      <div className={classes.paper}>
        <Typography component="h1" variant="h5">
          Sign up
        </Typography>
        <form className={classes.form} noValidate onSubmit={(e) => signUp(e)}>

          <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            label="Full Name"
            autoFocus
            value={fullName}
            onChange={(e) => setFullName(e.target.value)}
          />

          <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            label="User Name"
            name='user_name'
            error={userName && userName.length < 6}
            value={userName}
            onChange={(e) => setUserName(e.target.value)}
          />

          <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            label="Email"
            error={email && !emailValidate.test(email)}
            value={email}
            onChange={(e) => setEmail(e.target.value)}
          />

          <FormHelperText
            children={email && !emailValidate.test(email) ? 'Email không hợp lệ' : ''}
          />

          <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            label="Number Phone"
            error={phone && !phoneNumberValidate.test(phone)}
            value={phone}
            onChange={(e) => setPhone(e.target.value)}
          />

          <FormHelperText
            children={phone && !phoneNumberValidate.test(phone) ? 'Số điện thoại không hợp lệ' : ''}
          />

          <TextField
            variant="outlined"
            margin="normal"
            required
            focused
            fullWidth
            defaultValue='yyyy-mm-dd'
            type='date'
            label="Birthday"
            value={birthday}
            onChange={(e) => setBirthday(e.target.value)}
          />

          <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            label="Password"
            error={password && password.length < 8}
            type="password"
            name="password"
            autoComplete={false}
            value={password}
            onChange={(e) => setPassword(e.target.value)}
          />

          <FormHelperText
            children={password && password.length < 8 ? 'Mật khẩu có độ dài tối thiểu 8 ký tự' : ''}
          />

          <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            label="Confirm Password"
            type="password"
            error={rePassword && password !== rePassword}
            value={rePassword}
            onChange={(e) => setRePassword(e.target.value)}
          />

          <Button
            type="submit"
            fullWidth
            variant="contained"
            color="primary"
            className={classes.submit}
          >
            Register
          </Button>
        </form>
      </div>
    </Container>
  )
};

export default Register;