import react, { useEffect } from "react";
import '../styles/globals.css'
import store from "../redux/store";
import { Provider } from 'react-redux';
import Layout from "../components/common/Layout";
import { ChakraProvider } from "@chakra-ui/react";

function MyApp({Component, pageProps}) {

  return (
    <Provider store={store}>
      <ChakraProvider>
        <Layout>
          <Component {...pageProps} />
        </Layout>
      </ChakraProvider>
    </Provider>
  );
}

export default MyApp;
