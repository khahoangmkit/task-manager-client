import React, { useEffect, useState } from 'react';
import {
  Box,
  Button,
  Center,
  Container,
  Flex,
  Spacer,
  Table,
  Tbody,
  Td,
  Text,
  Th,
  Thead,
  toast,
  Tr, useToast
} from "@chakra-ui/react";
import { getDateFromTime } from "../../utils/formatTime";
import { useRouter } from "next/router";
import userApi from "../../services/userApi";
import { getMessage, isError, isErrorResponse } from "../../utils/typeof";
import { useSelector } from "react-redux";
import ModalComponent from "../ModalComponent";
import adminApi from "../../services/adminApi";

const ListProject = (props) => {

  const role = useSelector(state => state.user?.role);

  const toast = useToast();
  const router = useRouter();

  const [listProject, setListProject] = useState([]);
  const [openUpdateModal, setOpenUpdateModal] = useState(false);
  const [listDepartment, setListDepartment] = useState([]);

  const [initValueForm, setInitValueForm] = useState({})


  const getListDepartment = () => {
    adminApi.getListDepartment()
      .then((res) => {
        if (isErrorResponse(res)) {
          console.log(getMessage(res));
        } else {
          setListDepartment(res.data);
        }
      })
  };

  const getListProject = () => {
    userApi.getFullProject().then((res) => {
      if (isErrorResponse(res)) {
        console.log(getMessage(res));
      } else {
        setListProject(res.data);
      }
    })
  };

  const onCreateProject = () => {
    setInitValueForm({
      name: '',
      description: '',
      department: '',
      goal: '',
      deadline: '',
    });
    setOpenUpdateModal(true);
  };

  useEffect(() => {
    getListProject();
    getListDepartment();
  }, []);

  const validate = (value) => {
    const errors = {};
    if (!value.name) {
      errors.name = "Tên dự án không được bỏ trống";
    }

    if (!value.goal) {
      errors.goal = "Mục tiêu của dự án không được bỏ trống";
    }

    if (!value.description) {
      errors.description = "Miêu tả dự án không được bỏ trống";
    }

    if (!value.deadline) {
      errors.deadline = "Hạn chót dự án không được bỏ trống";
    }

    if (!value.department) {
      errors.department = "Phòng ban phụ trách dự án không được bỏ trống";
    }

    return errors;
  };

  const submitCreateProject = (project) => {
    userApi.createProject(project)
      .then((res) => {
        if(res.code !== 201) {
          toast({
            title: "Lỗi",
            description: getMessage(res),
            status: "error",
            duration: 3000,
            isClosable: true,
          })
        } else {
          const list = [...listProject];
          list.push(res.data);
          toast({
            title: "Thành công.",
            description: "Thêm dự án mới thành công.",
            status: "success",
            duration: 3000,
            isClosable: true,
          })
          setListProject(list);
        }
      })
      .catch((err) => {
        toast({
          title: "Lỗi",
          description: getMessage(err),
          status: "error",
          duration: 3000,
          isClosable: true,
        })
      })
    return null;
  };
  return (
    <Container
      w={'container.xl'}
      maxWidth={'100%'}
      h={'100vh'}
      pt={'50px'}
    >
      <Container
        w={'100%'}
        maxWidth={'100%'}
        position={'sticky'}
        top={'60px'}
        bg={'gray.50'}
      >
        <Flex
          direction={'row'}
          w={'90%'}
          maxWidth={'100%'}
        >
          <Box>
            <Text fontSize={'5xl'} fontWeight={'bold'} p={2}>Danh sách dự án</Text>
          </Box>
          <Spacer/>
          {
            role === 'president' &&
            <Center>
              <Button
                colorScheme={'twitter'}
                onClick={() => onCreateProject(true)}
                mr={4}
              >
                Thêm dự án
              </Button>
            </Center>}
        </Flex>
      </Container>

      <Container
        w={'100%'}
        maxWidth={'90%'}
        mt={3}
      >
        <Table
          size={'sm'}
          colorScheme={'gray'}
        >
          <Thead>
            <Tr>
              <Th>STT</Th>
              <Th>Tên dự án</Th>
              <Th>Chỉ tiêu</Th>
              <Th>Hạn chót</Th>
            </Tr>
          </Thead>
          <Tbody>
            {
              listProject && listProject.map((project, index) => (
                <Tr
                  key={`task-${index}`}
                  _hover={{
                    background: 'gray.100'
                  }}
                >
                  <Td>{index + 1}</Td>
                  <Td>
                    <Text
                      cursor={'pointer'}
                      color={'#0088ff'}
                      onClick={() => {
                        router.push(`/project/${project._id}`)
                      }}>
                      {project.name}
                    </Text>
                  </Td>
                  <Td>{project.goal}</Td>
                  <Td>{getDateFromTime(1613719730000)}</Td>
                </Tr>
              ))
            }
          </Tbody>
        </Table>
      </Container>

      <ModalComponent
        title={'Tạo dự án mới'}
        titleSubmit={'Tạo dự án'}
        openForm={openUpdateModal}
        initValue={initValueForm}
        closeForm={() => setOpenUpdateModal(false)}
        validateValue={validate}
        fields={[
          {key: 'name', title: 'Tên dự án:'},
          {key: 'goal', title: 'Mục tiêu của dự án:'},
          {key: 'description', type: 'textarea', title: 'Miêu tả dự án:'},
          {key: 'deadline', title: 'Hạn chót của dự án:'},
          {
            key: 'department',
            display: 'name',
            title: 'Phòng ban:',
            placeholder: 'Chọn phòng ban',
            type: 'selector',
            data: listDepartment
          },
        ]}
        onSubmit={submitCreateProject}
      />
    </Container>
  )
};

export default ListProject;