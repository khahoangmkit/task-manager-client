import React, { useEffect, useMemo, useState } from 'react';
import { useSelector } from "react-redux";
import {
  Box,
  Button,
  Center,
  Container,
  Flex,
  Input,
  Spacer,
  Table, Tbody, Td,
  Text, Th,
  Thead,
  Tr,
  useToast
} from "@chakra-ui/react";
import userApi from "../../services/userApi";
import { getMessage, isError, isErrorResponse } from "../../utils/typeof";
import { getParamByRouter } from "../../utils/history";
import { useRouter } from "next/router";
import { getDateFromTime } from "../../utils/formatTime";
import ModalComponent from "../../components/ModalComponent";
import adminApi from "../../services/adminApi";

const ProjectDetail = (props) => {

  const router = useRouter();

  const role = useSelector(state => state.user?.role);

  const projectId = useMemo(() => getParamByRouter(router, 'project_id'), [router]);
  const user = useSelector(state => state.user.infoUser || null);

  const toast = useToast();

  const [isSubmit, setIsSubmit] = useState(false);

  const [project, setProject] = useState({});
  const [listTask, setListTask] = useState([]);
  const [openUpdateModal, setOpenUpdateModal] = useState(false);
  const [openCreateModal, setOpenCreateModal] = useState(false);
  const [listDepartment, setDepartment] = useState([]);

  const [initValueForm, setInitValueForm] = useState({})

  const getListDepartment = () => {
    adminApi.getListDepartment()
      .then((res) => {
        if (isErrorResponse(res)) {
          console.log(getMessage(res));
        } else {
          setDepartment(res.data);
        }
      })
  }

  const submitUpdateProject = (projectInfo) => {
    const newProject = {
      ...projectInfo,
      project_id: project?._id
    }

    userApi.updateProject(newProject)
      .then((res) => {
        if(isErrorResponse(res)) {
          toast({
            title: "Lỗi",
            description: "Cập nhật dự án thất bại.",
            status: "error",
            duration: 3000,
            isClosable: true,
          })
        } else {
          toast({
            title: "Thành công.",
            description: "Cập nhật dự án thành công.",
            status: "success",
            duration: 3000,
            isClosable: true,
          })
          setProject(res.data);
        }
      })
      .catch((err) => {
        toast({
          title: "Lỗi",
          description: getMessage(err),
          status: "error",
          duration: 3000,
          isClosable: true,
        })
      })
  };

  useEffect(() => {
    if (projectId) {
      userApi.getProjectById(projectId)
        .then((res) => {
          if (isErrorResponse(res)) {
            console.log(getMessage(res));
          } else {
            setProject(res.data);
          }
        })
        .catch((err) => {
          console.log(getMessage(err))
        })
    }

    userApi.getFullTask().then((res) => {
      if (isErrorResponse(res)) {
        console.log(getMessage(res));
      } else {
        setListTask(res.data);
      }
    })

    getListDepartment();
  }, [projectId]);

  const validate = (value) => {
    const errors = {};
    if (!value.name) {
      errors.name = "Tên dự án không được bỏ trống";
    }

    if (!value.goal) {
      errors.goal = "Mục tiêu của dự án không được bỏ trống";
    }

    if (!value.description) {
      errors.description = "Miêu tả dự án không được bỏ trống";
    }

    if (!value.deadline) {
      errors.deadline = "Hạn chót dự án không được bỏ trống";
    }

    if (!value.department) {
      errors.department = "Phòng ban phụ trách dự án không được bỏ trống";
    }

    return errors;
  };

  const onOpenCreateTask = () => {
    setInitValueForm({
      name: '',
      user: '',
      description: '',
      goal: '',
      deadline: '',
      note: '',
    })
    setOpenCreateModal(true);
  };

  const onOpenUpdateProject = () => {
    setInitValueForm({
      name: project.name,
      goal: project.goal,
      deadline: getDateFromTime(project.deadline),
      description: project.description,
      department: project.department?._id
    })
    setOpenUpdateModal(true);
  };

  const handleUpdate = () => {
    const newUser = {
      ...user
    }
    userApi.updateProfile(newUser)
      .then((res) => {
        if (isErrorResponse(res) && isError(res)) {
          console.log(getMessage(res));
          toast({
            title: "Lỗi",
            description: "Cập nhập tiến độ thất bại.",
            status: "error",
            duration: 3000,
            isClosable: true,
          })
        } else {
          toast({
            title: "Đã lưu",
            render: "top",
            description: "Cập nhập tiến độ thành công.",
            status: "success",
            duration: 3000,
            isClosable: true,
          })
        }
      })
      .catch((err) => {
        console.log(getMessage(err));
      })
  }
  return (
    <Container
      w={'container.xl'}
      maxWidth={'100%'}
      h={'100vh'}
      pt={'50px'}
    >
      <Container
        w={'100%'}
        maxWidth={'100%'}
        position={'sticky'}
        top={'60px'}
        bg={'gray.50'}
      >
        <Flex
          direction={'row'}
          w={'90%'}
          maxWidth={'100%'}
        >
          <Box>
            <Text fontSize={'5xl'} fontWeight={'bold'} p={2}>Chi tiết dự án</Text>
          </Box>
          <Spacer/>
          {
            role === 'manager' &&
            <Center>
              <Button
                colorScheme={'twitter'}
                onClick={() => onOpenCreateTask()}
                mr={4}
              >
                Thêm nhiệm vụ
              </Button>
            </Center>
          }

          {
            role === 'president' &&
            <Center>
              <Button
                colorScheme={'twitter'}
                onClick={onOpenUpdateProject}
                mr={4}
              >
                Cập nhập dự án
              </Button>
            </Center>
          }
        </Flex>
      </Container>

      <Container
        w={'100%'}
        maxWidth={'90%'}
      >

        <Flex w={'100%'} py={'1rem'} direction={'row'}>
          <Text
            w={'10rem'}
            fontSize={'md'}
            fontWeight={'bold'}
          >
            Tên dự án:
          </Text>
          <Text maxWidth={'50rem'}>{project.name}</Text>
        </Flex>

        <Flex w={'100%'} direction={'row'}>
          <Text
            w={'10rem'}
            fontSize={'md'}
            fontWeight={'bold'}
            m={'auto 0'}
          >
            Phòng thực hiện:
          </Text>
          <Text maxWidth={'50rem'}>{project.department?.name}</Text>
        </Flex>

        <Flex w={'100%'} direction={'row'}>
          <Text
            w={'10rem'}
            fontSize={'md'}
            fontWeight={'bold'}
            m={'auto 0'}
          >
            Mô tả dự án:
          </Text>
          <Text maxWidth={'50rem'}>{project.description}</Text>
        </Flex>
      </Container>

      <Container
        w={'100%'}
        maxWidth={'90%'}
        mt={'2rem'}
      >
        <Box bg={'gray.100'}>
          <Text fontSize={'2xl'} fontWeight={'bold'} p={2}>Danh sách công việc của dự án</Text>
        </Box>
        <Container
          w={'100%'}
          maxWidth={'100%'}
        >
          <Table
            size={'sm'}
            // variant={'striped'}
            colorScheme={'gray'}
          >
            <Thead>
              <Tr>
                <Th>STT</Th>
                <Th>Tên công việc</Th>
                <Th>Hoàn thành</Th>
                <Th>Hạn chót</Th>
              </Tr>
            </Thead>
            <Tbody>
              {
                listTask && listTask.map((task, index) => (
                  <Tr
                    key={`task-${index}`}
                    _hover={{
                      background: 'gray.100'
                    }}
                  >
                    <Td>{index + 1}</Td>
                    <Td>
                      <Text
                        cursor={'pointer'}
                        color={'#0088ff'}
                        onClick={() => {
                          router.push(`/task/${task.name}`)
                        }}>
                        {task.name}
                      </Text>
                    </Td>
                    <Td>{task.done} / {task.goal}</Td>
                    <Td>{getDateFromTime(1613719730000)}</Td>
                  </Tr>
                ))
              }
            </Tbody>
          </Table>
        </Container>
      </Container>
      <ModalComponent
        title={'Cập nhập dự án'}
        titleSubmit={'Cập nhập'}
        openForm={openUpdateModal}
        initValue={initValueForm}
        closeForm={() => setOpenUpdateModal(false)}
        validateValue={validate}
        fields={[
          {key: 'name', title: 'Tên nhiệm vụ:'},
          {key: 'goal', title: 'Mục tiêu của nhiệm vụ:'},
          {key: 'description',type: 'textarea', title: 'Miêu tả nhiệm vụ:'},
          {key: 'deadline', title: 'Hạn chót:(dd/mm/yyyy)'},
          {
            key: 'department',
            display: 'name',
            title: 'Phòng ban phụ trách:',
            placeholder: 'Chọn phòng phụ trách',
            type: 'selector',
            data: listDepartment
          },
        ]}
        onSubmit={submitUpdateProject}
      />
    </Container>
  )
};

export default ProjectDetail;