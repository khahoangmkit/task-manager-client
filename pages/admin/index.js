import React, { Fragment, useRef, useState } from "react";
import {
  Box,
  Button,
  Center,
  Container,
  createStandaloneToast,
  Flex,
  FormControl,
  FormErrorMessage,
  FormLabel,
  Input,
  Modal,
  ModalBody,
  ModalCloseButton,
  ModalContent, ModalFooter,
  ModalHeader,
  ModalOverlay,
  Spacer,
  Table,
  Tbody,
  Td,
  Text, Textarea,
  Th,
  Thead,
  Tr, useDisclosure
} from "@chakra-ui/react";
import { useFormik } from "formik";
import adminApi from "../../services/adminApi";
import { getMessage, isError, isErrorResponse } from "../../utils/typeof";

const Admin = (props) => {

  const toast = createStandaloneToast();
  const {isOpen, onOpen, onClose} = useDisclosure();
  const initialRef = useRef();

  const [listDepartment, setListDepartment] = useState([]);

  const validate = (value) => {
    const errors = {};
    if(!value.name) {
      errors.name = "Tên phòng ban không được bỏ trống";
    }

    if(!value.mission) {
      errors.mission = "Sứ mệnh của phòng ban không được bỏ trống";
    }

    if(!value.location) {
      errors.location = "Vị trí của phòng ban không được bỏ trống"
    }

    return errors;
  };

  const formik = useFormik({
    initialValues: {
      name: '',
      mission: '',
      location: '',
    },
    validate,
    onSubmit: values => {

    },
  });

  return (
    <Container
      w={'container.xl'}
      maxWidth={'100%'}
      h={'100vh'}
      pt={'100px'}
    >
      <Flex
        direction={'row'}
        w={'90%'}
        maxWidth={'100%'}

      >
        <Box>
          <Text fontSize={'5xl'} fontWeight={'bold'} p={2}>Danh sách quyền</Text>
        </Box>
        <Spacer/>
        <Center>
          <Button
            colorScheme={'twitter'}
            onClick={onOpen}
            mr={4}
          >
            Thêm phòng ban
          </Button>
        </Center>
      </Flex>

      <Container
        w={'100%'}
        maxWidth={'90%'}
        mt={3}
      >
        <Table
          size={'sm'}
          colorScheme={'gray'}
        >
          <Thead>
            <Tr>
              <Th>STT</Th>
              <Th>Tên quyền</Th>
              <Th>Mô tả</Th>
              <Th></Th>
            </Tr>
          </Thead>
          <Tbody>
            {
              listDepartment && listDepartment.map((role, index) => (
                <Tr
                  key={`role-${index}`}
                  _hover={{
                    background: 'gray.100'
                  }}
                >
                  <Td>{index + 1}</Td>
                  <Td><Text cursor={'pointer'}>{role.name}</Text></Td>
                  <Td>{role.description}</Td>
                  <Td
                    cursor={'pointer'}
                    color={'red'}
                    _hover={{
                      textDecoration: "underline"
                    }}
                    onClick={() => deleteRole({name: role.name, id: role._id})}
                  >
                    Xóa
                  </Td>
                </Tr>
              ))
            }
          </Tbody>
        </Table>
      </Container>
      <Modal
        closeOnOverlayClick={false}
        initialFocusRef={initialRef}
        isOpen={isOpen}
        onClose={onClose}
        colorScheme={'cyan'}
        size={'xl'}
        motionPreset={'scale'}
      >
        <ModalOverlay/>
        <ModalContent opacity={'1 !important'}>
          <ModalHeader>Thêm phòng ban mới</ModalHeader>
          <ModalCloseButton/>

          <ModalBody>
            <Container w={'container.lg'} maxWidth={'100%'}>
              <form onSubmit={formik.handleSubmit}>

                <FormControl isInvalid={formik.errors?.name}>
                  <FormLabel htmlFor="name">Tên quyền: </FormLabel>
                  <Input
                    id="name"
                    value={formik.values.name}
                    onChange={formik.handleChange}
                    ref={initialRef}
                    placeholder="name"/>
                  <FormErrorMessage position={'absolute'}>{formik.errors?.name}</FormErrorMessage>
                </FormControl>

                <FormControl mt={'3rem'} isInvalid={formik.errors?.description}>
                  <FormLabel htmlFor="description">Mô tả quyền: </FormLabel>
                  <Textarea
                    id="description"
                    value={formik.values.description}
                    onChange={formik.handleChange}
                    placeholder="description"/>
                  <FormErrorMessage position={'absolute'}>{formik.errors?.description}</FormErrorMessage>
                </FormControl>
              </form>
            </Container>
          </ModalBody>

          <ModalFooter>
            <Button
              colorScheme={'red'}
              onClick={onClose}
              mx={'2rem'}
            >
              Hủy
            </Button>
            <Button
              colorScheme={'blue'}
              isLoading={props.isSubmitting}
              onClick={formik.handleSubmit}
              type="submit"
            >
              Tạo quyền
            </Button>
          </ModalFooter>
        </ModalContent>
      </Modal>
    </Container>
  )
};

export default Admin;