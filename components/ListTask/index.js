import React, { useEffect, useState } from 'react';
import { Box, Container, Table, Tbody, Td, Text, Th, Thead, Tr } from "@chakra-ui/react";
import userApi from "../../services/userApi";
import { getDateFromTime } from "../../utils/formatTime";
import { useRouter } from "next/router";
import { getMessage, isErrorResponse } from "../../utils/typeof";

const ListTask = (props) => {

  const [listTask, setListTask] = useState([]);

  const router = useRouter();

  useEffect(() => {
    userApi.getFullTask().then((res) => {
      if (isErrorResponse(res)) {
        console.log(getMessage(res));
      } else {
        setListTask(res.data);
      }
    })
  }, []);

  return (
    <Container
      w={'container.xl'}
      maxWidth={'100%'}
      h={'100vh'}
      pt={'50px'}
    >
      <Box>
        <Text fontSize={'5xl'} fontWeight={'bold'} p={'3rem'}>Danh sách công việc</Text>
      </Box>
      <Container
        w={'100%'}
        maxWidth={'90%'}
      >
        <Table
          size={'sm'}
          // variant={'striped'}
          colorScheme={'gray'}
        >
          <Thead>
            <Tr>
              <Th>STT</Th>
              <Th>Tên công việc</Th>
              <Th>Hoàn thành</Th>
              <Th>Hạn chót</Th>
            </Tr>
          </Thead>
          <Tbody>
            {
              listTask && listTask.map((task, index) => (
                <Tr
                  key={`task-${index}`}
                  _hover={{
                    background: 'gray.100'
                  }}
                >
                  <Td>{index + 1}</Td>
                  <Td>
                    <Text
                      cursor={'pointer'}
                      color={'#0088ff'}
                      onClick={() => {
                        router.push(`/task/${task.name}`)
                      }}>
                      {task.name}
                    </Text>
                  </Td>
                  <Td>{task.done} / {task.goal}</Td>
                  <Td>{getDateFromTime(1613719730000)}</Td>
                </Tr>
              ))
            }
          </Tbody>
        </Table>
      </Container>
    </Container>
  )
};

export default ListTask;