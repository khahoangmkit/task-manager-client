import React from 'react';
import TopBar from "../TopBar";

const Layout = ({children}) => {
  return (
    <div className="container">
      <TopBar/>
      {children}
    </div>
  )
};

export default Layout;