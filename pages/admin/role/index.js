import React, { useEffect, useRef, useState } from 'react';
import {
  Box,
  Button, Center,
  Container, createStandaloneToast, Flex,
  FormControl,
  FormErrorMessage,
  FormLabel,
  Input, Spacer, Table, Tbody, Td, Text,
  Textarea, Th, Thead, Tr, useDisclosure
} from "@chakra-ui/react";
import {
  Modal,
  ModalOverlay,
  ModalContent,
  ModalHeader,
  ModalFooter,
  ModalBody,
  ModalCloseButton,
} from "@chakra-ui/react"
import { Field, Form, Formik, FormikProps, useFormik } from 'formik';
import adminApi from "../../../services/adminApi";
import { getMessage, isError, isErrorResponse } from "../../../utils/typeof";

const Role = (props) => {

  const toast = createStandaloneToast();

  const initialRef = useRef();
  const [listRole, setListRole] = useState([]);

  const getRoles = () => {
    adminApi.getListRole()
      .then((res) => {
        if (isErrorResponse(res) && isError(res)) {
          console.log(res);
        } else {
          setListRole(res.data);
        }
      })
      .catch((err) => {
        console.log(getMessage(err))
      })
  }

  useEffect(() => {
    getRoles();
  }, []);

  const validate = (value) => {
    const errors = {};
    if (!value.name) {
      errors.name = "Tên quyền không được bỏ trống"
    } else if (!/^\S*$/.test(value.name.trim())) {
      errors.name = "Tên quyền không được chứa khoảng trắng"
    }

    if (!value.description) {
      errors.description = "Mô tả quyền không dược bỏ trống"
    }

    return errors;
  }

  const {isOpen, onOpen, onClose} = useDisclosure();

  const formik = useFormik({
    initialValues: {
      name: '',
      description: '',
    },
    validate,
    onSubmit: values => {
      adminApi.createRole(values).then((res) => {
        if (res.code === 201) {
          toast({
            title: "Đã xong",
            render: "top",
            description: "Tạo quyền thành công",
            status: "success",
            duration: 3000,
            isClosable: true,
          })
          formik.handleReset();
          getRoles();
          onClose();
        } else if (isErrorResponse(res) || isError(res)) {
          toast({
            title: "Thất bại",
            render: "top",
            description: getMessage(res),
            status: "error",
            duration: 3000,
            isClosable: true,
          })
        }
      });
    },
  });

  const deleteRole = (param) => {
    const {name, id} = param;

    adminApi.deleteRole(id)
      .then((res) => {
        console.log(res, 'res ===')
        if (isErrorResponse(res) || isError(res)) {
          console.log(getMessage(res), 'asdasd')
        } else {
          toast({
            title: "Đã xóa",
            render: "top",
            description: "Xóa quyền thành công",
            status: "success",
            duration: 3000,
            isClosable: true,
          })
          setListRole(res.data);
        }
      })
      .catch((err) => {
        console.log(getMessage(err))
      })

  }

  return (
    <Container
      w={'container.xl'}
      maxWidth={'100%'}
      h={'100vh'}
      pt={'100px'}
    >
      <Container
        w={'100%'}
        maxWidth={'100%'}
        position={'sticky'}
        top={'60px'}
        bg={'gray.50'}
      >
        <Flex
          direction={'row'}
          w={'90%'}
          maxWidth={'100%'}
        >
          <Box>
            <Text fontSize={'5xl'} fontWeight={'bold'} p={2}>Danh sách quyền</Text>
          </Box>
          <Spacer/>
          <Center>
            <Button
              colorScheme={'twitter'}
              onClick={onOpen}
              mr={4}
            >
              Thêm quyền
            </Button>
          </Center>
        </Flex>
      </Container>
      <Container
        w={'100%'}
        maxWidth={'90%'}
        mt={3}
      >
        <Table
          size={'md'}
          // variant={'striped'}
          colorScheme={'gray'}
        >
          <Thead>
            <Tr  bg={'gray.300'}>
              <Th>STT</Th>
              <Th>Tên quyền</Th>
              <Th>Mô tả</Th>
              <Th></Th>
            </Tr>
          </Thead>
          <Tbody>
            {
              listRole && listRole.map((role, index) => (
                <Tr
                  key={`role-${index}`}
                  _hover={{
                    background: 'gray.100'
                  }}
                >
                  <Td>{index + 1}</Td>
                  <Td><Text cursor={'pointer'}>{role.name}</Text></Td>
                  <Td>{role.description}</Td>
                  <Td
                    cursor={'pointer'}
                    color={'red'}
                    _hover={{
                      textDecoration: "underline"
                    }}
                    onClick={() => deleteRole({name: role.name, id: role._id})}
                  >
                    Xóa
                  </Td>
                </Tr>
              ))
            }
          </Tbody>
        </Table>
      </Container>
      <Modal
        closeOnOverlayClick={false}
        initialFocusRef={initialRef}
        isOpen={isOpen}
        onClose={onClose}
        colorScheme={'cyan'}
        size={'xl'}
        motionPreset={'scale'}
      >
        <ModalOverlay/>
        <ModalContent opacity={'1 !important'}>
          <ModalHeader>Thêm quyền mới</ModalHeader>
          <ModalCloseButton/>

          <ModalBody>
            <Container w={'container.lg'} maxWidth={'100%'}>
              <form onSubmit={formik.handleSubmit}>

                <FormControl isInvalid={formik.errors?.name}>
                  <FormLabel htmlFor="name">Tên quyền: </FormLabel>
                  <Input
                    id="name"
                    value={formik.values.name}
                    onChange={formik.handleChange}
                    ref={initialRef}
                    placeholder="name"/>
                  <FormErrorMessage position={'absolute'}>{formik.errors?.name}</FormErrorMessage>
                </FormControl>

                <FormControl mt={'3rem'} isInvalid={formik.errors?.description}>
                  <FormLabel htmlFor="description">Mô tả quyền: </FormLabel>
                  <Textarea
                    id="description"
                    value={formik.values.description}
                    onChange={formik.handleChange}
                    placeholder="description"/>
                  <FormErrorMessage position={'absolute'}>{formik.errors?.description}</FormErrorMessage>
                </FormControl>
              </form>
            </Container>
          </ModalBody>

          <ModalFooter>
            <Button
              colorScheme={'red'}
              onClick={onClose}
              mx={'2rem'}
            >
              Hủy
            </Button>
            <Button
              colorScheme={'blue'}
              isLoading={props.isSubmitting}
              onClick={formik.handleSubmit}
              type="submit"
            >
              Tạo quyền
            </Button>
          </ModalFooter>
        </ModalContent>
      </Modal>
    </Container>
  )
};

export default Role;