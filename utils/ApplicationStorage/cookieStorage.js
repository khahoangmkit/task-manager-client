const KEY_TOKEN = 'jwt';
export const IS_LOCALHOST =
  typeof window !== 'undefined' && !window.location.host.includes('3003');

export function setCookie(cname, cvalue, exdays) {
  let d = new Date();
  d.setTime(d.getTime() + exdays * 24 * 60 * 60 * 1000);
  let expires = `expires=${d.toUTCString()}`;
  if (typeof window !== 'undefined') {
    document.cookie = `${cname}=${cvalue};${expires};path=/`;
  }
}

export function getCookie(cname) {
  if (typeof window !== 'undefined') {
    let name = cname + '=';
    let decodedCookie = decodeURIComponent(document.cookie);
    let ca = decodedCookie.split(';');
    for (let i = 0; i < ca.length; i++) {
      let c = ca[i];
      while (c.charAt(0) === ' ') {
        c = c.substring(1);
      }
      if (c.indexOf(name) === 0) {
        return c.substring(name.length, c.length);
      }
    }
  }
  return '';
}

export function deleteCookie(name) {
  if (typeof window !== 'undefined') {
    document.cookie = `${name}=;expires=Thu, 01 Jan 1970 00:00:01 GMT; path=/;`;
  }
}
export const storagePersis = {
  timeout: null,
  getItem: (key) => {
    return new Promise((resolve, reject) => {
      if (process?.browser) {
        if (storageToken.getItem()) {
          resolve(window.localStorage.getItem(key));
        } else {
          window.localStorage.removeItem(key);
          resolve(null);
        }
      }
    });
  },
  setItem: (key, item) => {
    return new Promise((resolve, reject) => {
      if (process?.browser) {
        window.localStorage.setItem(key, item);
      }
      resolve(item);
    });
  },
  removeItem: (key) => {
    return new Promise((resolve, reject) => {
      if (process?.browser) {
        window.localStorage.removeItem(key);
      }
    });
  }
};

export const storageToken = {
  getItem: () => {
    if (typeof window !== 'undefined') {
      return getCookie(KEY_TOKEN);
    }
  },
  setItem: (item) => {
    const exdays = 60;
    let d = new Date();
    d.setTime(d.getTime() + exdays * 24 * 60 * 60 * 1000);
    let expires = `expires=${d.toUTCString()}`;
    if (typeof window !== 'undefined') {
      if (IS_LOCALHOST) {
        document.cookie = `${KEY_TOKEN}=${item};${expires};path=/`;
      } else {
        document.cookie = `${KEY_TOKEN}=${item};${expires};path=/;promo_shown=1;SameSite=Lax;Secure;`;
      }
    }
    return item;
  },
  removeItem: () => {
    if (typeof window !== 'undefined') {
      if (IS_LOCALHOST) {
        document.cookie = `${KEY_TOKEN}=;expires=Thu, 01 Jan 1970 00:00:01 GMT; path=/`;
      } else {
        document.cookie = `${KEY_TOKEN}=;expires=Thu, 01 Jan 1970 00:00:01 GMT;;path=/;promo_shown=1;SameSite=Lax;Secure;`;
      }
    }
  }
};
