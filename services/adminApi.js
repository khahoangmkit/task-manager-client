import base from "./base";

export default {
  getListRole: () => {
    return base.get('/role/list');
  },
  createRole: (role) => {
    return base.post('/role/create', role);
  },
  deleteRole: (roleId) => {
    return base.delete(`/role/delete/${roleId}`)
  },
  getListDepartment: () => {
    return base.get('/department/list');
  },
  createDepartment: (department) => {
    return base.post('/department/create', department);
  },
  removeDepartment: (departmentId) => {
    return base.delete(`/department/delete/${departmentId}`);
  },
  updateDepartment: (department) => {
    return base.post('/department/update', department);
  },
  getListUser: () => {
    return base.get('/user/list');
  },
  updateUser: (user) => {
    return base.post('/user/update', user);
  },
  createUser: (user) => {
    return base.post('/user/register', user);
  },
  removeUser: (userId) => {
    return base.delete(`/user/delete/${userId}`)
  }
}