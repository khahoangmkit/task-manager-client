import React, { useState } from 'react';
import { useSelector } from "react-redux";
import SideBar from "../SideBar";
import { Grid, Box, Heading, Text, Container, Flex, Center } from "@chakra-ui/react";
import { ChevronDownIcon, HamburgerIcon } from "@chakra-ui/icons";


const TopBar = (props) => {
  const [openSideBar, setOpenSideBar] = useState(false);

  const fullName = useSelector(state => state.user.infoUser?.full_name || null);

  const handleClose = () => {
    setOpenSideBar(false);
  };

  return (
    <Container
      w={'100%'}
      maxWidth={'100%'}
      bg={'#0088ff'}
      position={'fixed'}
      top={0}
      h={'60px'}
    >
      {
        openSideBar && <SideBar close={handleClose}/>
      }
      <Grid
        w={'container.xl'}
        templateColumns="100px auto 300px"
        gap={3}
        m={'0 auto'}
      >
        <Box color={'#fff'}>
          <HamburgerIcon
            onClick={() => setOpenSideBar(true)}
            ml={5}
            w={30}
            h={50}/>
        </Box>
        <Container
          ml={0}
        >
          <Text
            fontSize={'4xl'}
            cursor={'pointer'}
            color={'#fff'}
            w={'700px'}
          >
            Hệ thống quản lý chỉ tiêu nhiệm vụ
          </Text>
        </Container>
        <Box margin={'auto 0'} color={'#fff'}>
          {
            fullName &&
            <Text cursor={'pointer'} fontSize={18} color={'#fff'}>
              {fullName}
              <ChevronDownIcon/>
            </Text>
          }
        </Box>
      </Grid>
    </Container>
  );
};

export default TopBar;