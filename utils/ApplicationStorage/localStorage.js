export const setItem = ({ name, value }) => {
  if (typeof window !== 'undefined') {
    localStorage.setItem(name, value);
  }
};

export const getItem = (name) => {
  if (typeof window !== 'undefined') {
    return localStorage.getItem(name);
  }
};

export const removeItem = (name) => {
  if (typeof window !== 'undefined') {
    window.localStorage.removeItem(name);
  }
};
