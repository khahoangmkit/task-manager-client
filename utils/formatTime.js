import { isNumber, isString } from "./typeof";

export const getValueFromDate = (time) => {
  if (time && time.length !== 10) {
    let creationTime = new Date(time);

    let day = creationTime.getDate();
    let month = creationTime.getMonth() + 1;

    if (day < 10) {
      day = `0${day}`;
    }

    if (month < 10) {
      month = `0${month}`;
    }

    return `${creationTime.getFullYear()}-${month}-${day}`;
  }
};

export const getDateFromTime = (time) => {
  let creationTime = new Date();
  try {
    if (isNumber(time)) {
      creationTime = new Date(time) / 1000;
    } else if (isString(time)) {
      creationTime = new Date(parseInt(time, 10));
    }

    creationTime = new Date(time);
  } catch (e) {
    console.log(e);
  }

  let day = creationTime.getDate();
  let month = creationTime.getMonth() + 1;

  if (day < 10) {
    day = `0${day}`;
  }

  if (month < 10) {
    month = `0${month}`;
  }

  return `${day}/${month}/${creationTime.getFullYear()}`;
};