import React, { useEffect, useState } from 'react';
import userApi from "../../services/userApi";
import { useRouter } from "next/router";
import cookie from 'cookie-cutter';
import { useDispatch } from "react-redux";
import { setInfo, setRole } from "../../redux/actions/user";
import {
  Button,
  Container,
  createStandaloneToast,
  Grid,
  Input,
  InputGroup,
  InputRightElement,
  Text,
  Wrap,
  WrapItem
} from "@chakra-ui/react";
import { ViewIcon, ViewOffIcon } from "@chakra-ui/icons";
import { getMessage, isErrorResponse } from "../../utils/typeof";


const Login = (props) => {

  const [showPassword, setShowPassword] = useState(false);
  const [userName, setUserName] = useState('');
  const [password, setPassword] = useState('');

  const toast = createStandaloneToast();

  const router = useRouter();
  const dispatch = useDispatch();

  const signIn = (e) => {
    e.preventDefault();
    userApi.login({
      user_name: userName,
      password
    })
      .then((res) => {
        if (res.data.code !== 200) {
          toast({
            title: "Đăng nhập thất bại",
            render: "top",
            description: "Tên đăng nhập hoặc mật khẩu không đúng",
            status: "error",
            duration: 3000,
            isClosable: true,
          })
        } else {
          const token = res.data.token;
          cookie.set('token', token, {expires: 60 * 3600});


          userApi.getMyInfo().then((res) => {
            if (isErrorResponse(res)) {
              console.log(getMessage(res));
            } else {
              const {user, role} = res.data;
              dispatch(setInfo(user));
              dispatch(setRole(role));
              router.push('/');
            }
          })
        }
      })
      .catch((err) => {
        console.log(err.message)
      })
  };

  const handleClick = () => {
    setShowPassword(!showPassword);
  }

  return (
    <Container
      w={'100%'}
      mt={'100px'}
    >
      <Text
        fontSize={32}
        p={'20px 0'}
        textAlign={'center'}
      >
        Đăng nhập vào hệ thống
      </Text>
      <Wrap spacing={'10px'}>
        <WrapItem w={'100%'}>
          <Text m={'auto 0'} fontSize={'lg'} w={'200px'}>User Name:</Text>
          <Input
            placeholder={'Enter your email'}
            size={'md'}
            onChange={(e) => setUserName(e.target.value)}
            value={userName}
          />
        </WrapItem>

        <WrapItem w={'100%'}>
          <Text m={'auto 0'} fontSize={'lg'} w={'190px'}>Password:</Text>
          <InputGroup size="md">
            <Input
              type={showPassword ? "text" : "password"}
              placeholder="Enter password"
              size={'md'}
              onChange={(e) => setPassword(e.target.value)}
              value={password}
            />
            <InputRightElement width="3rem">
              <Button h="1.7rem" mr={'0.5rem'} size="md" onClick={handleClick}>
                {showPassword ? <ViewIcon/> : <ViewOffIcon/>}
              </Button>
            </InputRightElement>
          </InputGroup>
        </WrapItem>

        <WrapItem
          w={'100%'}
        >
          <Button
            colorScheme='blue'
            textAlign={'center'}
            m={'2rem auto'}
            onClick={signIn}
          >
            Đăng nhập
          </Button>
        </WrapItem>

      </Wrap>
    </Container>
  )
};

export function getServerSideProps(context) {
  const cookie = context.req.headers.cookie;

  if (cookie) {
    const [, token] = cookie.split('=');

    if (token) {
      context.res.writeHead(302, {Location: '/'});
      context.res.end();
    }
  }

  return {props: {}};
}

export default Login;