import { SET_ROLE, SET_TOKEN, SET_USER } from "../types/user";

export const setInfo = (infoUser) => (dispatch) => {
  return dispatch({
    type: SET_USER,
    payload: infoUser
  })
};

export const setToken = (token) => (dispatch) => {
  return dispatch({
    type: SET_TOKEN,
    payload: token
  })
};

export const setRole = (role) => (dispatch) => {
  return dispatch({
    type: SET_ROLE,
    payload: role
  })
}