const localStorageKey = 'redux-store-tab-sync';

function isPromise(v) {
  return v && typeof v.then === 'function';
}

const promiseMiddleware = ({dispatch}) => next => action => {
  if (isPromise(action.payload)) {
    action.payload.then(
      res => {
        if (typeof res === 'undefined') {
          return;
        }
        let tempAction = action;
        tempAction.payload = res;
        dispatch(tempAction);
      },
      err => {
        // browserHistory.push('/500');
        // history.push('/500');
        let tempAction = action;
        tempAction.payload = err;
        dispatch(action);
      }
    ).catch(error => {
      dispatch({
        type: 'ERROR',
        payload: error
      });
    });
  } else {
    next(action);
  }
};

const localStorageListener = store => {
  return () => {
    const indentifiedAction = JSON.parse(localStorage.getItem(localStorageKey));

    if (
      indentifiedAction &&
      indentifiedAction.action &&
      indentifiedAction.action.type
    ) {
      if (indentifiedAction.tabID !== getCurrentTabID()) {
        store.dispatch(indentifiedAction);
        localStorage.removeItem(localStorageKey);
      }
    }
  };
};

export {
  promiseMiddleware,
  localStorageListener
}