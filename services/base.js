import { isErrorResponse, isFunction } from "../utils/typeof";
import authenticateInstance from '../axiosConfig';

const base = {
  authenticateInstance,
  login: (body) => {
    return authenticateInstance.post('/user/login', body);
  },
  signup: (body) => {
    return authenticateInstance.post('/user/register', body);
  },
  get: (url, {cb, nonToken = false, returnToResponse = null} = {}) => {
    return authenticateInstance
      .get(url)
      .then((rs) => {
        if (rs.data) {
          if (rs.data.code && rs.data.code === 200 && rs.data.body) {
            return rs.data.body;
          } else {
            return rs.data;
          }
        } else {
          return {
            code: 500,
            message: 'not found data'
          };
        }
      })
      .catch((error) => {
        if (error.response) {
          if (error.response.data) {
            const data = error.response.data || {};
            return {
              code: data.code || data.status,
              message: data.message || data.error
            };
          } else {
            return {
              code: error.response.status,
              message: (error && error.message) || 'Internal Server Error'
            };
          }
        }

        return {
          code: 500,
          message: (error && error.message) || 'Internal Server Error'
        };
      })
      .then((rs) => {
        if (typeof (cb) === 'function') {
          cb(rs);
        }
        return rs;
      });
  },
  post: (url, body, {cb, nonToken = false, returnToResponse = null} = {}) => {
    return authenticateInstance
      .post(url, body)
      .then((rs) => {
        if (rs.data) {
          if (rs.data.code && rs.data.code === 200 && rs.data.body) {
            return rs.data.body;
          } else {
            return rs.data;
          }
        } else {
          return {
            code: 500,
            message: 'not found data'
          };
        }
      })
      .catch((error) => {
        if (error.response) {
          if (error.response.data) {
            const data = error.response.data || {};
            return {
              code: data.code || data.status,
              message: data.message || data.error
            };
          } else {
            return {
              code: error.response.status,
              message: (error && error.message) || 'Internal Server Error'
            };
          }
        }

        return {
          code: 500,
          message: (error && error.message) || 'Internal Server Error'
        };
      })
      .then((rs) => {
        if (isFunction(cb)) {
          cb(rs);
        }
        return rs;
      });
  },
  delete: (url, params, body) => {
    return authenticateInstance
      .delete(url, {
        params: params,
        data: body
      })
      .then((rs) => {
        if (!isErrorResponse(rs)) {
          return rs.data.body;
        } else {
          return rs.data;
        }
      })
      .catch((error) => {
        if (error.response) {
          if (error.response.data) {
            const data = error.response.data || {};
            return {
              code: data.code || data.status,
              message: data.message || data.error
            };
          } else {
            return {
              code: error.response.status,
              message: (error && error.message) || 'Internal Server Error'
            };
          }
        }

        return {
          code: 500,
          message: (error && error.message) || 'Internal Server Error'
        };
      });
  }
}

export default base;
