import axios from "axios";
import cookie from 'cookie-cutter';



const instance = axios.create({
  baseURL: 'http://localhost:3003'
});

instance.interceptors.request.use(
  (config) => {
    const token = cookie.get('token') || null
    if (token) {
      config.headers.Authorization = `Bearer ${token}`;
    } else {
      config.headers.Authorization = null;
    }
    return config;
  },
  (error) => {
    return Promise.reject(error);
  }
);

export default instance;
