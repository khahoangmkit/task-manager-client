import React, { useEffect, useState } from 'react';
import {
  Button,
  Container,
  FormControl, FormErrorMessage, FormLabel, Input, Modal,
  ModalBody,
  ModalCloseButton,
  ModalContent, ModalFooter,
  ModalHeader,
  ModalOverlay, Select, Textarea, useDisclosure
} from "@chakra-ui/react";
import { useFormik } from "formik";
import adminApi from "../../services/adminApi";
import { element } from "prop-types";

const ModalComponent = (props) => {

    const {
      openForm,
      closeForm,
      title,
      initValue,
      validateValue,
      fields,
      onSubmit,
      titleSubmit
    } = props;

    const {isOpen, onOpen, onClose} = useDisclosure();

    useEffect(() => {
      if (openForm) onOpen();
    }, [openForm])

    const formik = useFormik({
      initialValues: initValue,
      validate: validateValue,
      enableReinitialize: true,
      onSubmit: values => {
        onSubmit(values);
        closeForm();
        onClose();
      }
    });

    const onCloseModel = () => {
      closeForm();
      formik.handleReset();
      onClose();
    };

    return (
      <Modal
        closeOnOverlayClick={false}
        isOpen={isOpen}
        onClose={onCloseModel}
        colorScheme={'cyan'}
        size={'xl'}
        motionPreset={'scale'}
      >
        <ModalOverlay/>
        <ModalContent opacity={'1 !important'}>
          <ModalHeader>{title}</ModalHeader>
          <ModalCloseButton/>
manager
          <ModalBody>
            <Container w={'container.lg'} maxWidth={'100%'}>
              <form onSubmit={formik.handleSubmit}>


                {
                  fields && fields.map((field, index) => {
                      if (field.type === 'selector') {
                        return (
                          <FormControl
                            mt={index !== 0 ? '2rem' : ''}
                            key={`field-${index}`}
                            isInvalid={formik.errors[field.key]}
                          >
                            <FormLabel htmlFor={field.key}>{field.title}</FormLabel>
                            <Select
                              id={field.key}
                              onChange={formik.handleChange}
                              placeholder={field.placeholder}>
                              {
                                field && field.data.map((option, index) => {
                                  return (
                                    <option
                                      value={option._id}
                                      key={`option-${index}`}
                                      selected={formik.values[field.key] === option._id ? true : false}
                                    >
                                      {option[field.display]}
                                    </option>)
                                })
                              }
                            </Select>
                          </FormControl>
                        )
                      } else {
                        return (
                          <FormControl
                            mt={index !== 0 ? '2rem' : ''}
                            key={`field-${index}`}
                            isInvalid={formik.errors[field.key]}
                          >
                            <FormLabel htmlFor={field.key}>{field.title}</FormLabel>
                            {
                              field.type === 'textarea' ?
                                <Textarea
                                  id={field.key}
                                  value={formik.values[field.key]}
                                  onChange={formik.handleChange}
                                  placeholder={field.key}
                                /> :
                                <Input
                                  id={field.key}
                                  value={formik.values[field.key]}
                                  onChange={formik.handleChange}
                                  placeholder={field.key}/>
                            }
                            <FormErrorMessage position={'absolute'}>{formik.errors[field.key]}</FormErrorMessage>
                          </FormControl>
                        )
                      }
                    }
                  )
                }
              </form>
            </Container>
          </ModalBody>

          <ModalFooter>
            <Button
              colorScheme={'red'}
              onClick={onCloseModel}
              mx={'2rem'}
            >
              Hủy
            </Button>
            <Button
              colorScheme={'blue'}
              isLoading={props.isSubmitting}
              onClick={formik.handleSubmit}
              type="submit"
            >
              {titleSubmit}
            </Button>
          </ModalFooter>
        </ModalContent>
      </Modal>
    )
  }
;

export default ModalComponent;