import React, { useState } from 'react';
import {
  Button, Center,
  Container,
  createStandaloneToast, FormControl, FormErrorMessage, FormLabel,
  Input,
  InputGroup,
  InputRightElement,
  Text,
  Wrap,
  WrapItem
} from "@chakra-ui/react";
import { ViewIcon, ViewOffIcon } from "@chakra-ui/icons";
import userApi from "../../../services/userApi";
import cookie from "cookie-cutter";
import { getMessage, isErrorResponse } from "../../../utils/typeof";
import { setInfo } from "../../../redux/actions/user";
import { useFormik } from "formik";
import { useSelector } from "react-redux";
import { useRouter } from "next/router";

const ChangePassword = (props) => {

  const toast = createStandaloneToast();
  const router = useRouter();

  const userId = useSelector(state => state.user.infoUser._id);

  // const [oldPassword, setOldPassword] = useState('');
  // const [newPassword, setNewPassword] = useState('');
  // const [confirmPassword, setConfirmPassword] = useState('');

  const validate = (values) => {
    const errors = {};

    if (!values.oldPassword) {
      errors.oldPassword = 'Vui lòng nhập mật khẩu cũ';
    }

    if (!values.newPassword) {
      errors.newPassword = 'Vui lòng nhập mật khẩu mới';
    }

    if (!values.confirmPassword) {
      errors.confirmPassword = 'Vui lòng nhập lại mật khẩu mới';
    } else if (values.newPassword !== values.confirmPassword) {
      errors.confirmPassword = 'Mật khẩu không khớp';
    }

    return errors;
  };
  const formik = useFormik({
    initialValues: {
      oldPassword: '',
      newPassword: '',
      confirmPassword: '',
    },
    validate,
    onSubmit: values => {
      changePassword(values);
    }
  })

  const changePassword = (params) => {
    // e.preventDefault();
    console.log(params)
    userApi.changePassword({
      user_id: userId,
      old_password: params.oldPassword,
      new_password: params.newPassword
    })
      .then((res) => {
        if (isErrorResponse(res)) {
          toast({
            title: "Lỗi",
            render: "top",
            description: getMessage(res),
            status: "error",
            duration: 3000,
            isClosable: true,
          })
        } else {
          toast({
            title: "Thành công",
            render: "top",
            description: "Đã cập nhập mật khẩu mới",
            status: "success",
            duration: 3000,
            isClosable: true,
          })
          setTimeout(() => router.push('/'), 1000)
        }
      })
      .catch((err) => {
        console.log(err.message);
      })
  };
  return (
    <Container
      w={'100%'}
      maxWidth={'50%'}
      mt={'100px'}
    >
      <Text
        fontSize={32}
        p={'20px 0'}
        textAlign={'center'}
      >
        Đổi mật khẩu
      </Text>
      <form onSubmit={formik.handleSubmit}>
        <FormControl mt={'2rem'} isInvalid={formik.errors.oldPassword}>
          <FormLabel htmlFor={'oldPassword'}>Mật khẩu cũ:</FormLabel>
          <Input
            id={'oldPassword'}
            type={'password'}
            value={formik.values.oldPassword}
            onChange={formik.handleChange}
            placeholder={'Nhập mật khẩu cũ'}/>
          <FormErrorMessage position={'absolute'}>{formik.errors.oldPassword}</FormErrorMessage>
        </FormControl>

        <FormControl mt={'2rem'} isInvalid={formik.errors.newPassword}>
          <FormLabel htmlFor={'newPassword'}>Mật khẩu mới</FormLabel>
          <Input
            id={'newPassword'}
            type={'password'}
            value={formik.values.newPassword}
            onChange={formik.handleChange}
            placeholder={'Nhập mật khẩu mới'}/>
          <FormErrorMessage position={'absolute'}>{formik.errors.newPassword}</FormErrorMessage>
        </FormControl>

        <FormControl mt={'2rem'} isInvalid={formik.errors.confirmPassword}>
          <FormLabel htmlFor={'confirmPassword'}>Nhập lại mật khẩu mới</FormLabel>
          <Input
            id={'confirmPassword'}
            type={'password'}
            value={formik.values.confirmPassword}
            onChange={formik.handleChange}
            placeholder={'Nhập lại mật khẩu mới'}/>
          <FormErrorMessage position={'absolute'}>{formik.errors.confirmPassword}</FormErrorMessage>
        </FormControl>

        <Center mt={'3rem'}>
          <Button
            colorScheme={'blue'}
            onClick={formik.handleSubmit}
            type="submit"

          >
            Đổi mật khẩu
          </Button>
        </Center>

      </form>
      {/*<Wrap spacing={'10px'}>*/}
      {/*  <WrapItem w={'100%'}>*/}
      {/*    <Text m={'auto 0'} fontSize={'lg'} w={'16rem'}>Mật khẩu cũ:</Text>*/}
      {/*    <Input*/}
      {/*      placeholder={'Enter old password'}*/}
      {/*      size={'md'}*/}
      {/*      onChange={(e) => setOldPassword(e.target.value)}*/}
      {/*      value={oldPassword}*/}
      {/*    />*/}
      {/*  </WrapItem>*/}

      {/*  <WrapItem w={'100%'}>*/}
      {/*    <Text m={'auto 0'} fontSize={'lg'} w={'16rem'}>Mật khẩu mới:</Text>*/}
      {/*    <Input*/}
      {/*      type={"password"}*/}
      {/*      placeholder="Enter new password"*/}
      {/*      size={'md'}*/}
      {/*      onChange={(e) => setNewPassword(e.target.value)}*/}
      {/*      value={newPassword}*/}
      {/*    />*/}
      {/*  </WrapItem>*/}

      {/*  <WrapItem w={'100%'}>*/}
      {/*    <Text m={'auto 0'} fontSize={'lg'} w={'16rem'}>Nhập lại mật khẩu:</Text>*/}
      {/*    <Input*/}
      {/*      type={"password"}*/}
      {/*      placeholder="Confirm password"*/}
      {/*      size={'md'}*/}
      {/*      onChange={(e) => setConfirmPassword(e.target.value)}*/}
      {/*      value={confirmPassword}*/}
      {/*    />*/}
      {/*  </WrapItem>*/}


      {/*  <WrapItem*/}
      {/*    w={'100%'}*/}
      {/*  >*/}
      {/*    <Button*/}
      {/*      colorScheme='blue'*/}
      {/*      textAlign={'center'}*/}
      {/*      m={'2rem auto'}*/}
      {/*      onClick={changePassword}*/}
      {/*    >*/}
      {/*      Đăng nhập*/}
      {/*    </Button>*/}
      {/*  </WrapItem>*/}

      {/*</Wrap>*/}
    </Container>
  )
};

export default ChangePassword;