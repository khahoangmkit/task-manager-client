import Head from 'next/head'
import styles from '../styles/Home.module.css'
import { useDispatch, useSelector } from "react-redux";
import React, { useEffect } from "react";
import ListTask from "../components/ListTask";
import ListProject from "../components/ListProject";

export default function Home() {

  const name = useSelector(state => state.user.infoUser?.full_name);

  return (
    <div className={styles.container}>
      <Head>
        <title>Hệ thống quản lý chỉ tiêu nhiệm vụ</title>
        <link rel="icon" href="/favicon.png"/>
      </Head>

      <div style={{width: '100%'}}>
        {/*<ListTask />*/}
        <ListProject/>
      </div>
    </div>
  )
}
