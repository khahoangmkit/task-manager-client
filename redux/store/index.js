import { createStore, applyMiddleware } from "redux"
import thunkMiddleware from "redux-thunk";
import { composeWithDevTools } from 'redux-devtools-extension/developmentOnly';

import reducers from '../reducers';

function saveToLocalStorage(state) {
  try {
    const serializedState = JSON.stringify(state);
    localStorage.setItem('root', serializedState)
  } catch (e) {
    console.log(e);
  }
}

function loadFromLocalStorage() {
  try {
    const serializedState = typeof window !== 'undefined'
      ? localStorage.getItem('root') : null;
    if(!serializedState) return undefined;
    return JSON.parse(serializedState);
  } catch (e) {
    console.log(e);
    return undefined;
  }
}

const persistedState = loadFromLocalStorage();

const store = createStore(
  reducers,
  persistedState,
  composeWithDevTools(
    applyMiddleware(thunkMiddleware)
  )
)


store.subscribe(() => {saveToLocalStorage(store.getState())})

export default store;