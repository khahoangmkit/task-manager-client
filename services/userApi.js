import base from "./base";

export default {
  getMyInfo: () => {
    return base.get('/user/me');
  },
  getListUser: () => {
    return base.get('/user/show');
  },
  updateProfile: (user) => {
   return base.post('/user/update',user);
  },
  changePassword: (params) => {
    return base.post('/user/update/password', params);
  },
  login: (body) => {
    return base.login(body);
  },
  signUp: (body) => {
    return base.signup(body);
  },
  getFullTask: () => {
    return base.get('/task/list');
  },
  getTaskByName: (taskName) => {
    return base.get(`/task/${taskName}`);
  },
  updateTask: (task) => {
    return base.post('/task/update/progress', task);
  },
  getFullProject: () => {
    return base.get('/project/list')
  },
  getProjectById: (projectId) => {
    return base.get(`/project/${projectId}`);
  },
  createProject: (project) => {
    return base.post(`/project/create`, project);
  },
  updateProject: (newProject) => {
    return base.post(`/project/update`, newProject);
  }
}