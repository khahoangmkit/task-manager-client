import React, { useEffect, useState } from 'react';
import {
  Box,
  Button,
  Center,
  Container,
  createStandaloneToast,
  Flex,
  Spacer,
  Table,
  Tbody,
  Td,
  Text,
  Th,
  Thead,
  Tr
} from "@chakra-ui/react";
import ModalComponent from "../../../components/ModalComponent";
import adminApi from "../../../services/adminApi";
import { getMessage, isErrorResponse } from "../../../utils/typeof";

const User = (props) => {

  const toast = createStandaloneToast();

  const [listUsers, setListUsers] = useState([]);
  const [listDepartment, setListDepartment] = useState([]);
  const [listRole, setListRole] = useState([]);

  const [userSelected, setUserSelected] = useState(null);
  const [openUpdateModal, setOpenUpdateModal] = useState(false);
  const [openCreateModal, setOpenCreateModal] = useState(false);
  const [initValueForm, setInitValueForm] = useState({});

  const getListDepartment = () => {
    adminApi.getListDepartment()
      .then((res) => {
        if (isErrorResponse(res)) {
          console.log(getMessage(res));
        } else {
          setListDepartment(res.data);
        }
      })
  }

  const getListUser = () => {
    adminApi.getListUser()
      .then((res) => {
        console.log(res)
        if (isErrorResponse(res)) {
          console.log(getMessage(res));
        } else {
          setListUsers(res.data);
        }
      })
  }

  const getListRole = () => {
    adminApi.getListRole()
      .then((res) => {
        if (isErrorResponse(res)) {
          console.log(getMessage(res));
        } else {
          setListRole(res.data);
        }
      })
  }

  useEffect(() => {
    getListDepartment();
    getListRole();
    adminApi.getListUser()
      .then((res) => {
        console.log(res)
        if (isErrorResponse(res)) {
          console.log(getMessage(res));
        } else {
          setListUsers(res.data);
        }
      })
  }, []);

  const validate = (value) => {
    const errors = {};
    if (!value.email) {
      errors.email = "Email không được bỏ trống";
    } else if (!/^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/g.test(value.email)) {
      errors.email = "Vui lòng điền đúng định dạng email.";
    }

    if (!value.full_name) {
      errors.full_name = "Tên người dùng không được bỏ trống.";
    }

    if (!value.number_phone) {
      errors.number_phone = "Số điện thoại không được bỏ trống"
    }

    if (!value.role) {
      errors.role = "Chọn chức vụ của người dùng."
    }

    if (!value.department) {
      errors.department = "Vui lòng chọn phòng làm việc của người dùng"
    }

    return errors;
  };

  const removeUser = (params) => {
    const userId = params.id;

    adminApi.removeUser(userId)
      .then((res) => {
        if (isErrorResponse(res)) {
          getListDepartment();
          toast({
            title: "Đã xong",
            render: "top",
            description: "Xóa người dùng thành công",
            status: "success",
            duration: 3000,
            isClosable: true,
          })
        }
      })
      .catch((err) => {
        console.log(err);
      })

  };


  const updateUser = (user) => {
    setUserSelected(user._id);
    setInitValueForm({
      full_name: user.full_name,
      email: user.email,
      number_phone: user.number_phone,
      department: user.department?._id,
      role: user.role?._id,
    });
    setOpenUpdateModal(true);
  };

  const createUser = () => {
    setInitValueForm({
      full_name: '',
      email: '',
      number_phone: '',
      department: '',
      role: '',
    })
    setOpenCreateModal(true);
  };

  const submitUpdateUser = (user) => {
    adminApi.updateUser(user)
      .then((res) => {
        getListUser();
        toast({
          title: "Đã xong",
          render: "top",
          description: "Đã cập nhật thông tin người dùng.",
          status: "success",
          duration: 3000,
          isClosable: true,
        })
      })
      .catch((err) => {
        console.log(err);
      })
  };

  const submitCreateUser = (user) => {
    console.log(user)
    adminApi.createUser(user)
      .then((res) => {
        if (isErrorResponse(res)) {
          getListUser();
          toast({
            title: "Đã xong",
            render: "top",
            description: "Thêm người dùng thành công.",
            status: "success",
            duration: 3000,
            isClosable: true,
          })
        }
      })
      .catch((err) => {
        console.log(getMessage(err))
      })
  };


  return (
    <Container
      w={'container.xl'}
      maxWidth={'100%'}
      h={'100vh'}
      pt={'100px'}
    >
      <Container
        w={'100%'}
        maxWidth={'100%'}
        position={'sticky'}
        top={'60px'}
        bg={'gray.100'}
      >
        <Flex
          direction={'row'}
          w={'95%'}
          maxWidth={'100%'}
          justify={'space-between'}
        >
          <Box>
            <Text
              fontSize={'5xl'}
              fontWeight={'bold'}
              p={2}
            >
              Danh sách người dùng
            </Text>
          </Box>
          <Spacer/>
          <Center>
            <Button
              colorScheme={'twitter'}
              onClick={() => createUser()}
              mr={4}
            >
              Thêm người dùng
            </Button>
          </Center>
        </Flex>
      </Container>
      <Container
        w={'100%'}
        maxWidth={'95%'}
        mt={3}
      >
        <Table
          size={'md'}
          colorScheme={'gray'}
        >
          <Thead>
            <Tr>
              <Th>STT</Th>
              <Th>Tên người dùng</Th>
              <Th>Phòng ban</Th>
              <Th>Email</Th>
              <Th>SĐT</Th>

              <Th></Th>
              <Th></Th>
            </Tr>
          </Thead>
          <Tbody>
            {
              listUsers && listUsers.map((user, index) => (
                <Tr
                  key={`department-${index}`}
                  _hover={{
                    background: 'gray.100'
                  }}
                >
                  <Td>{index + 1}</Td>
                  <Td><Text cursor={'pointer'}>{user.full_name}</Text></Td>
                  <Td><Text cursor={'pointer'}>{user.department?.name}</Text></Td>
                  <Td>{user.email}</Td>
                  <Td>{user.number_phone}</Td>
                  <Td
                    cursor={'pointer'}
                    color={'green'}
                    _hover={{
                      textDecoration: "underline"
                    }}
                    onClick={() => updateUser(user)}
                  >Sửa</Td>
                  <Td
                    cursor={'pointer'}
                    color={'red'}
                    _hover={{
                      textDecoration: "underline"
                    }}
                    onClick={() => removeUser({id: user._id})}
                  >
                    Xóa
                  </Td>
                </Tr>
              ))
            }
          </Tbody>
        </Table>
      </Container>
      <ModalComponent
        title={'Cập nhập phòng ban'}
        titleSubmit={'Cập nhập'}
        openForm={openUpdateModal}
        initValue={initValueForm}
        closeForm={() => setOpenUpdateModal(false)}
        validateValue={validate}
        fields={[
          {key: 'email', title: 'Email người dùng:'},
          {key: 'full_name', title: 'Họ và tên người dùng:'},
          {key: 'number_phone', title: 'Số điện thoại người dùng:'},
          {key: 'role', display: 'name', title: 'Quyền:', placeholder: 'Chọn quyền', type: 'selector', data: listRole},
          {
            key: 'department',
            display: 'name',
            title: 'Phòng ban:',
            placeholder: 'Chọn phòng ban',
            type: 'selector',
            data: listDepartment
          },
        ]}
        onSubmit={submitUpdateUser}
      />

      <ModalComponent
        title={'Thêm người dùng mới'}
        titleSubmit={'Tạo người dùng'}
        openForm={openCreateModal}
        initValue={initValueForm}
        closeForm={() => setOpenCreateModal(false)}
        validateValue={validate}
        fields={[
          {key: 'email', title: 'Email người dùng:'},
          {key: 'full_name', title: 'Họ và tên người dùng:'},
          {key: 'number_phone', title: 'Số điện thoại người dùng:'},
          {key: 'role', display: 'name', title: 'Quyền:', placeholder: 'Chọn quyền', type: 'selector', data: listRole},
          {
            key: 'department',
            display: 'name',
            title: 'Phòng ban:',
            placeholder: 'Chọn trưởng phòng',
            type: 'selector',
            data: listDepartment
          },
        ]}
        onSubmit={submitCreateUser}
      />
    </Container>
  )
};

export default User;